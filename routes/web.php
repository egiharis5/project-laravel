<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@home');

Route::get('/register', 'AuthController@form');

Route::post('/welcome', 'AuthController@signin');

Route::get('/master', function() {
	return view('layout.master');
});


Route::get('/data-tables', function() {
	return view('table.data-table');
});

Route::get('/table', function() {
	return view('table.table');
});

//CRUD Genre
Route::get('/cast','Castcontroller@index');
Route::get('/cast/create','Castcontroller@create');
Route::post('/cast','Castcontroller@store');
Route::get('/cast/{cast_id}','Castcontroller@show');
Route::get('/cast/{cast_id}/edit','Castcontroller@edit');
Route::put('/cast/{cast_id}','Castcontroller@update');
Route::delete('/cast/{cast_id}','Castcontroller@destroy');
