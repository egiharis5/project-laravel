@extends('layout.master')
@section('judul')
Halaman Tambah Cast
@endsection
@section('isi')
<form action="/cast" method="POST">
            @csrf
            <div class="form-group">
                <label for="title">Nama cast</label>
                <input type="text" class="form-control" name="nama" id="nama1" placeholder="Masukkan nama cast">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">Umur cast</label>
                <input type="text" class="form-control" name="umur" id="umur1" placeholder="Masukkan umur cast">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">biografi</label>
                <input type="text" class="form-control" name="bio" id="bio1" placeholder="Masukkan Bio">
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
@endsection