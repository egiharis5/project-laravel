@extends('layout.master')
@section('judul')
Halaman Edit Cast ber-id {{$cast->id}}
@endsection
@section('isi')
<form action="/cast/{{$cast->id}}" method="POST">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="title">Nama cast</label>
                <input type="text" class="form-control" name="nama" value="{{$cast->nama}}" id="nama1" placeholder="Masukkan nama cast">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">Umur cast</label>
                <input type="text" class="form-control" name="umur" value="{{$cast->umur}}" id="umur1" placeholder="Masukkan umur cast">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">biografi</label>
                <input type="text" class="form-control" name="bio" value="{{$cast->bio}}" id="bio1" placeholder="Masukkan Bio">
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Edit</button>
        </form>
@endsection