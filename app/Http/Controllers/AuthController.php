<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{

	public function form(){
    	return view ('register');
    }
    public function signin(Request $request){
    	$nama1 = $request['nama1'];
    	$nama2 = $request['nama2'];
    	$gender = $request['gender'];
    	$nationality = $request['nationality'];
    	$bio = $request['bio'];
    	return view ('welcome',compact('nama1','nama2','gender','nationality','bio'));
    }
}
